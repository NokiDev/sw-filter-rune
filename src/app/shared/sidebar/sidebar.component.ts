/* Angular*/
import { Component, OnInit } from "@angular/core";
/* FontAwesome Icons */
import { faGem } from "@fortawesome/free-solid-svg-icons";
import { faGhost } from "@fortawesome/free-solid-svg-icons";
import { faFileImport } from "@fortawesome/free-solid-svg-icons";
import { faDatabase } from "@fortawesome/free-solid-svg-icons";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { faBook } from "@fortawesome/free-solid-svg-icons";
import { faRunning } from "@fortawesome/free-solid-svg-icons";
import { faEllipsisV } from "@fortawesome/free-solid-svg-icons";
import { faArrowUp, faArrowDown, faArrowLeft, faArrowRight } from "@fortawesome/free-solid-svg-icons";
import { AppComponent } from "../../app.component";


@Component({
  selector: "app-sidebar",
  templateUrl: "./sidebar.component.html",
  styleUrls: ["./sidebar.component.scss"],
})
export class SidebarComponent implements OnInit {
  expend = true;
  show = false;
  /* FontAwesome */
  gem = faGem;
  ghost = faGhost;
  file = faFileImport;
  database = faDatabase;
  bars = faBars;
  infos = faInfoCircle;
  book = faBook;
  runs = faRunning;
  ellipsis = faEllipsisV;
  arrowUp = faArrowUp;
  arrowDown = faArrowDown;
  arrowLeft = faArrowLeft
  arrowRight = faArrowRight;

  checked: boolean;

  constructor(public app: AppComponent) {}

  ngOnInit() {
    this.checked = JSON.parse(localStorage.getItem("isChecked"));
  }
  
  showHideNav() {
    this.show = !this.show;
  }
}
