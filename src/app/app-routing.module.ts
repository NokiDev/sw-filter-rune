/* NgModule and Route Import */
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
/* Component Import */
import { ImportDataComponent } from "./views/import-data/import-data.component";
import { MonstersComponent } from "./views/monsters/monsters.component";
import { RuneFilterComponent } from "./views/rune-filter/rune-filter.component";
import { GameInfoComponent } from "./views/game-info/game-info.component";
import { RunsComponent } from "./views/runs/runs.component";
import { Bj5Component } from './views/bj5/bj5.component';
import { InfosComponent } from './views/infos/infos.component';
import { ResumeComponent } from "./views/resume/resume.component";
import { IcaruComponent } from "./views/icaru/icaru.component";
import { LushenComponent } from "./views/lushen/lushen.component";

const routes: Routes = [
  { path: "", component: ImportDataComponent},
  { path: "resume", component: ResumeComponent},
  { path: "import", component: ImportDataComponent},
  { path: "monsters", component: MonstersComponent},
  { path: "rune-sell", component: RuneFilterComponent},
  { path: "game-info", component: GameInfoComponent},
  { path: "runs", component: RunsComponent},
  { path: "bj5", component: Bj5Component},
  { path: "icaru", component: IcaruComponent},
  { path: "lushen", component: LushenComponent},
  { path: "infos", component: InfosComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
