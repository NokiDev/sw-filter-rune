import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from '@angular/forms';
/* Angular Material */
import { MaterialModule } from "./module/material.module";
import { MAT_FORM_FIELD_DEFAULT_OPTIONS } from "@angular/material/form-field";
/* Routing */
import { AppRoutingModule } from "./app-routing.module";
/* Component */
import { AppComponent } from "./app.component";
import { GameInfoComponent } from './views/game-info/game-info.component';
import { RunsComponent } from './views/runs/runs.component';
import { Bj5Component } from './views/bj5/bj5.component';
import { InfosComponent } from './views/infos/infos.component';
import { ResumeComponent } from './views/resume/resume.component';
import { RuneFilterComponent } from "./views/rune-filter/rune-filter.component";
import { SidebarComponent } from "./shared/sidebar/sidebar.component";
import { ImportDataComponent } from "./views/import-data/import-data.component";
import { MonstersComponent } from "./views/monsters/monsters.component";
import { IcaruComponent } from './views/icaru/icaru.component';
/* NgxParser */
import { NgxCsvParserModule } from 'ngx-csv-parser';
/* FONT AWESOME */
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
/* Pipe */
import { ReplacePipe } from './core/replace.pipe';
import { MinuteSecondsPipe } from './core/format.pipe';
/* DialogBox */
import { loadingDialog } from './views/import-data/import-data.component';
import { loadingDialogCsv } from './views/runs/runs.component';
import { dialogParams } from './views/monsters/monsters.component';
import { FooterComponent } from './shared/footer/footer.component';
import { LushenComponent } from './views/lushen/lushen.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    ImportDataComponent,
    MonstersComponent, 
    RuneFilterComponent,
    GameInfoComponent,
    loadingDialog,
    dialogParams,
    RunsComponent,
    loadingDialogCsv,
    ReplacePipe,
    MinuteSecondsPipe,
    Bj5Component,
    InfosComponent,
    ResumeComponent,
    IcaruComponent,
    FooterComponent,
    LushenComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    FontAwesomeModule,
    NgxCsvParserModule,
    FormsModule,
    AppRoutingModule,
  ],
  providers: [
    {
      provide: MAT_FORM_FIELD_DEFAULT_OPTIONS,
      useValue: { appearance: "fill" },
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
