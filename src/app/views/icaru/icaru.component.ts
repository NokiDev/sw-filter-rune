/* Angular*/
import { Component, OnInit } from '@angular/core';
/* FontAwesome Icons */
import { faCheckCircle, faTimesCircle, faShieldAlt, faRunning, faStar, faHeart, faBahai, faPlusCircle, faEdit } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-icaru',
  templateUrl: './icaru.component.html',
  styleUrls: ['./icaru.component.scss']
})
export class IcaruComponent implements OnInit {
  /* Data */
  data: any = [];
  verde: any = [];
  building: any = [];
  icaru: any = [];
  secondIcaru: any = [];
  thirdIcaru: any = [];

  icaruId: number;
  secondIcaruId: number;
  thirdIcaruId: number;

  tricaruMinDef: number;
  tricaruDeterSetCount: number;
  tricaruFightSetCount: number;
  requiredSpd: boolean = false;
  actualMaxSpd: number;
  
  deterRunes: number;
  fightRunes: number;
  selectedRunes: any;
  verdeList: any = [];
  selectedVerde: any;
  showSelectVerde: boolean = false;
  /* FontAwesome Icons */
  star = faStar;
  checkCircle = faCheckCircle;
  timeCircle = faTimesCircle;
  shield = faShieldAlt;
  run = faRunning;
  heart = faHeart;
  bahai = faBahai;
  plusCircle = faPlusCircle;
  edit = faEdit;
  
  /* Verde */
  valid: boolean;
  validEhp: boolean;
  validVerdeDmg: boolean;
  hpVerde: number;
  eHpVerde: number;
  defVerde: number;
  spdVerde: number;
  atkVerde: number;
  crit_damageVerde: number;
  crit_rateVerde: number;
  verdeAtk: number;
  verdeBoostedAtk: number;
  bossReduction: number;
  finalDmg: number;
  /* First Icaru */
  hp: number;
  validDefMin: boolean;
  validSecondDefMin: boolean;
  validThirdDefMin: boolean;
  def: number;
  spd: number;
  atk: number;
  crit_damage: number;
  crit_rate: number;
  totalIcaruSpd: number;
  /* Second Icaru Stats */
  hpSecond: number;
  eHpSecond: number;
  defSecond: number;
  spdSecond: number;
  atkSecond: number;
  crit_damageSecond: number;
  crit_rateSecond: number;
  totalSecondIcaruSpd: number;
  /* Third Icaru Stats */
  hpThird: number;
  eHpThird: number;
  defThird: number;
  spdThird: number;
  atkThird: number;
  crit_damageThird: number;
  crit_rateThird: number;
  totalThirdIcaruSpd: number;
  /* Define Base Building Value */
  defBuilding = 0;
  atkBuilding = 0;
  critDamageBuilding = 0;
  fireAtkBuilding = 0;
  waterAtkBuilding = 0;
  spdBuilding = 0;
  hpBuilding = 0;
  /* Bonus Stats */
  determinationSet: number;
  fightSet: number;
  guildSkill: number;
  atkPercentBonus: number;
  atkDealtOnFire: number;
  /* Icaru Stats */
  icaruAtk: number;
  /* Skill Bonus */
  skillUp: number;
  skillCrit: number;
  /* Artifacts Bonus Damage */
  dmgBonusPerAtk: number;
  dmgBonusPerHp: number;
  dmgBonusPerDef: number;
  dmgBonusPerSpeed: number;
  dmgBonusFirstSkill: number;
  dmgDealtOnFire: number;
  
  isAtkBuilding = false;
  isHpBuilding = false;
  isWaterBuilding = false;
  isDefBuilding = false;
  isCritBuilding = false;
  isFireAtkBuilding = false;
  isWaterAtkBuilding = false;
  isSpeedBuilding = false;

  selectSpdBuilding = 0;
  selectFireBuilding = 0;
  selectWaterBuilding = 0;
  selectAttackBuilding = 0;
  selectDefBuilding = 0;
  selectHpBuilding = 0;
  selectCritBuilding = 0;

  /* Building Base Select Input */
  itemSpdBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 3},
    { id: 3, value: 5},
    { id: 4, value: 6},
    { id: 5, value: 8},
    { id: 6, value: 9},
    { id: 7, value: 11},
    { id: 8, value: 12},
    { id: 9, value: 14},
    { id: 10, value: 15},
  ];
  itemFireBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 3},
    { id: 2, value: 5},
    { id: 3, value: 7},
    { id: 4, value: 9},
    { id: 5, value: 11},
    { id: 6, value: 13},
    { id: 7, value: 15},
    { id: 8, value: 17},
    { id: 9, value: 19},
    { id: 10, value: 21},
  ];
  itemAttackBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 4},
    { id: 3, value: 6},
    { id: 4, value: 8},
    { id: 5, value: 10},
    { id: 6, value: 12},
    { id: 7, value: 14},
    { id: 8, value: 16},
    { id: 9, value: 18},
    { id: 10, value: 20},
  ];
  itemCritBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 5},
    { id: 3, value: 7},
    { id: 4, value: 10},
    { id: 5, value: 12},
    { id: 6, value: 15},
    { id: 7, value: 17},
    { id: 8, value: 20},
    { id: 9, value: 22},
    { id: 10, value: 25},
  ];
  itemDefBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 4},
    { id: 3, value: 6},
    { id: 4, value: 8},
    { id: 5, value: 10},
    { id: 6, value: 12},
    { id: 7, value: 14},
    { id: 8, value: 16},
    { id: 9, value: 18},
    { id: 10, value: 20},
  ];
  itemHpBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 4},
    { id: 3, value: 6},
    { id: 4, value: 8},
    { id: 5, value: 10},
    { id: 6, value: 12},
    { id: 7, value: 14},
    { id: 8, value: 16},
    { id: 9, value: 18},
    { id: 10, value: 20},
  ];
  itemWaterBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 3},
    { id: 2, value: 5},
    { id: 3, value: 7},
    { id: 4, value: 9},
    { id: 5, value: 11},
    { id: 6, value: 13},
    { id: 7, value: 15},
    { id: 8, value: 17},
    { id: 9, value: 19},
    { id: 10, value: 21},
  ];

  constructor() { }

  ngOnInit(): void {
    this.data = JSON.parse(localStorage.getItem("wizard_data"));
    this.building = JSON.parse(localStorage.getItem("wizard_building"));
    this.selectedVerde = JSON.parse(localStorage.getItem("verde_select"));

    this.buildingDisplay();
  
    this.determinationSet = 0;
    this.fightSet = 0;
    this.guildSkill = 5;
    this.atkPercentBonus = 0;
    this.skillCrit = 0;

    this.dmgBonusPerAtk = 0;
    this.dmgBonusPerHp = 0;
    this.dmgBonusPerDef = 0;
    this.dmgBonusPerSpeed = 0;
    this.dmgBonusFirstSkill = 0;
    this.dmgDealtOnFire = 0;

    this.tricaruDeterSetCount = 0;
    this.tricaruFightSetCount =  0;
    this.deterRunes = 0;
    this.fightRunes = 0;

    this.hp = 0;
    this.hpSecond = 0;
    this.hpThird = 0;
    this.hpVerde = 0;

    this.def = 0;
    this.defSecond = 0;
    this.defThird = 0;
    this.defVerde = 0;

    this.spd = 0;
    this.spdSecond = 0;
    this.spdThird = 0;
    this.spdVerde = 0;

    this.atk = 0;
    this.atkSecond = 0;
    this.atkThird = 0;
    this.atkVerde = 0;

    this.crit_rate = 0;
    this.crit_rateSecond = 0;
    this.crit_rateThird = 0;
    this.crit_rateVerde = 0;

    this.crit_damage = 0;
    this.crit_damageSecond = 0;
    this.crit_damageThird = 0;
    this.crit_damageVerde = 0;

    this.skillUp = 30;
    
    if(this.data){
      this.tricaruDisplay();
      this.verdehileDisplay();
    }
  
    this.onChange(event);
  }

  onChange(event) {
    this.requiredSpd = false;

    this.tricaruMinDef = 3300 - (736 * (this.selectDefBuilding + this.guildSkill)/ 100 + 736) - (736 * 0.08 * this.tricaruDeterSetCount);
    this.eHpVerde = (9885 + this.hpVerde + (9885 * (this.selectHpBuilding + this.guildSkill) / 100)) 
    * (1140 + (((505 + this.defVerde + ((505 + ((505 + this.defVerde) 
    * 0.08 * this.tricaruDeterSetCount)) * (this.selectDefBuilding + this.guildSkill) / 100)) * 3.5))) / 1000;
  
    let verdeDefBonus = Math.ceil(505 * ( 100 + this.selectDefBuilding + this.guildSkill + this.tricaruDeterSetCount * 8) /100 + this.defVerde);
    let verdeHpBonus = Math.ceil(9885 * ( 100 + this.selectHpBuilding + this.guildSkill ) /100 + this.hpVerde);
    let verdeSpdBonus = Math.ceil(99 * ( 100 + this.selectSpdBuilding + 28) /100 + this.spdVerde);
    let verdeAtkBonus = Math.ceil(812 * ( 100 + this.selectAttackBuilding + this.selectFireBuilding + this.guildSkill + this.tricaruFightSetCount * 8)/100 + this.atkVerde);

    this.verdeBoostedAtk = Math.ceil(1000/(1140 + 3.5 * Math.ceil(0.3 * 3605)) * (100 + this.dmgDealtOnFire )/100 * (4 * verdeAtkBonus *(100 + this.skillUp + 50 + this.crit_damageVerde 
      + this.selectCritBuilding + this.dmgBonusFirstSkill)/100 - (100 + this.skillUp)/100/10) + (this.dmgBonusPerDef * verdeDefBonus)/100 
      + (this.dmgBonusPerHp * verdeHpBonus)/100 + (this.dmgBonusPerSpeed * verdeSpdBonus)/100 + (this.dmgBonusPerAtk * verdeAtkBonus)/100);

    this.finalDmg = this.verdeBoostedAtk / 2;

    if(this.def >= this.tricaruMinDef){
      this.validDefMin = true;
    } else {
      this.validDefMin = false;
    }
    if(this.defSecond >= this.tricaruMinDef){
      this.validSecondDefMin = true;
    } else {
      this.validSecondDefMin = false;
    }
    if(this.defThird >= this.tricaruMinDef){
      this.validThirdDefMin = true;
    } else {
      this.validThirdDefMin = false;
    }
    if(this.eHpVerde >= 85000){
      this.validEhp = true;
    } else {
      this.validEhp = false;
    }
    if(this.finalDmg >= 3300){
      this.validVerdeDmg = true;
    } else {
      this.validVerdeDmg = false;
    }
    this.spdBuilding = this.selectSpdBuilding;
    this.totalIcaruSpd = 108 * (100 + this.spdBuilding + 28)/ 100 + this.spd;
    this.totalSecondIcaruSpd = 108 * (100 + this.spdBuilding + 28)/ 100 + this.spdSecond;
    this.totalThirdIcaruSpd = 108 * (100 + this.spdBuilding + 28)/ 100 + this.spdThird;

    this.actualMaxSpd = Math.max(this.totalIcaruSpd, this.totalSecondIcaruSpd, this.totalThirdIcaruSpd);

    if(this.totalIcaruSpd > 170 || this.totalSecondIcaruSpd > 170 || this.totalThirdIcaruSpd > 170){
      this.requiredSpd = !this.requiredSpd;
    }
  }
  buildingDisplay(){
    if(this.building){
      for(let building of this.building){
        if(building.lvl){
          if(building.lvl.type === "Speed"){
            this.selectSpdBuilding = building.lvl?.Speed;
            this.isSpeedBuilding = true;
          }
          if(building.lvl.type === "Fire Atk"){
            this.selectFireBuilding = building.lvl?.FireAttack;
            this.isFireAtkBuilding = true;
          }
          if(building.lvl.type === "Water Atk"){
            this.selectWaterBuilding = building.lvl?.WaterAttack;
            this.isWaterAtkBuilding = true;
          }
          if(building.lvl.type === "Def"){
            this.selectDefBuilding = building.lvl?.Def;
            this.isDefBuilding = true;
          } 
          if(building.lvl.type === "Atk"){
            this.selectAttackBuilding = building.lvl?.Attack;
            this.isAtkBuilding = true;
          }
          if(building.lvl.type === "Hp"){
            this.selectHpBuilding = building.lvl?.Hp;
            this.isHpBuilding = true;
          }
          if(building.lvl.type === "C.Dmg" ){
            this.selectCritBuilding = building.lvl?.CritDamage;
            this.isCritBuilding = true;
          }
        }
      }
    } else {
      this.building = [];
    }
  }
  verdehileDisplay(){
    /* Verdehile */
    this.verdeList = [];
    for(let index of this.data.unit_list){
      if(index.unit_master_id.name === "Verdehile"){
        this.verdeList.push(index);
        this.deterRunes = 0;
        this.fightRunes = 0;
        this.verde = this.selectedVerde;
        this.hpVerde =  this.verde?.unit_master_id.hp.toFixed() - 9885;
        this.defVerde = this.verde?.def.toFixed() - this.verde?.unit_master_id.base_def.toFixed();
        this.spdVerde = this.verde?.spd.toFixed() - this.verde?.unit_master_id.base_spd.toFixed();
        this.atkVerde = this.verde?.atk.toFixed() - this.verde?.unit_master_id.base_atk.toFixed();
        this.crit_rateVerde = this.verde?.critical_rate.toFixed() - this.verde?.unit_master_id.base_crit_rate.toFixed();
        this.crit_damageVerde = this.verde?.critical_damage.toFixed() - this.verde?.unit_master_id.base_crit_damage.toFixed();
        if(this.verde){
          for(let index of this.verde.runes){
            if(index.set_id.set_id == 20){
              this.deterRunes = this.deterRunes + 1;
            }
            if(index.set_id.set_id == 19){
              this.fightRunes = this.fightRunes + 1;
            }
          }
        }
        if(this.deterRunes == 2){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 1;
        }
        if(this.deterRunes == 4){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 2;
        }
        if(this.deterRunes == 6){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 3;
        }
        if(this.fightRunes == 2){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 1;
        }
        if(this.fightRunes == 4){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 2;
        }
        if(this.fightRunes == 6){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 3;
        }

        if(this.verde){
          for(let artifact of this.verde.artifacts){
            for(let sec_effect of artifact.sec_effects){
              if(sec_effect.id == 218){
                this.dmgBonusPerHp = sec_effect.value;
              }
              if(sec_effect.id == 219){
                this.dmgBonusPerAtk = sec_effect.value;
              }
              if(sec_effect.id == 220){
                this.dmgBonusPerDef = sec_effect.value;
              }
              if(sec_effect.id == 221){
                this.dmgBonusPerSpeed = sec_effect.value;
              }
              if(sec_effect.id == 400){
                this.dmgBonusFirstSkill = sec_effect.value
              }
              if(sec_effect.id == 305) {
                this.dmgDealtOnFire = sec_effect.value;
              }
            }
          }
        }
      }
    }
    this.onChange(event);
  }
  tricaruDisplay(){
    for(let index of this.data.unit_list){ 
      if(index.unit_master_id.name === "Icaru" && this.icaruId == undefined){
        this.icaru = index;
        this.hp =  index.unit_master_id.hp.toFixed() - this.icaru.unit_master_id.base_hp.toFixed();
        this.def = this.icaru.def.toFixed() - this.icaru.unit_master_id.base_def.toFixed();
        this.spd = this.icaru.spd.toFixed() - this.icaru.unit_master_id.base_spd.toFixed();
        this.atk = this.icaru.atk.toFixed() - this.icaru.unit_master_id.base_atk.toFixed();
        this.crit_rate = this.icaru.critical_rate.toFixed() - this.icaru.unit_master_id.base_crit_rate.toFixed();
        this.crit_damage = this.icaru.critical_damage.toFixed() - this.icaru.unit_master_id.base_crit_damage.toFixed();
        this.totalIcaruSpd = 108 * (100 + this.spdBuilding + 28)/ 100 + this.spd;
        this.icaruId = this.icaru.unit_id;

        for(let index of this.icaru.runes){
          if(index.set_id.set_id == 20){
            this.deterRunes = this.deterRunes + 1;
          }
          if(index.set_id.set_id == 19){
            this.fightRunes = this.fightRunes + 1;
          }
        }
        if(this.deterRunes == 2){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 1;
        }
        if(this.deterRunes == 4){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 2;
        }
        if(this.deterRunes == 6){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 3;
        }
        if(this.fightRunes == 2){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 1;
        }
        if(this.fightRunes == 4){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 2;
        }
        if(this.fightRunes == 6){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 3;
        }
      } else if(index.unit_master_id.name === "Icaru" && index.unit_id !== this.icaruId && this.secondIcaruId == undefined){
        this.fightRunes = 0;
        this.deterRunes = 0;
        this.secondIcaru = index;
        this.hpSecond =  index.unit_master_id.hp.toFixed() - this.secondIcaru.unit_master_id.base_hp.toFixed();
        this.defSecond = this.secondIcaru.def.toFixed() - this.secondIcaru.unit_master_id.base_def.toFixed();
        this.spdSecond = this.secondIcaru.spd.toFixed() - this.secondIcaru.unit_master_id.base_spd.toFixed();
        this.atkSecond = this.secondIcaru.atk.toFixed() - this.secondIcaru.unit_master_id.base_atk.toFixed();
        this.crit_rateSecond = this.secondIcaru.critical_rate.toFixed() - this.secondIcaru.unit_master_id.base_crit_rate.toFixed();
        this.crit_damageSecond = this.secondIcaru.critical_damage.toFixed() - this.secondIcaru.unit_master_id.base_crit_damage.toFixed();
        this.totalSecondIcaruSpd = 108 * (100 + this.spdBuilding + 28)/ 100 + this.spdSecond;
        this.secondIcaruId = this.secondIcaru.unit_id;
        for(let index of this.secondIcaru.runes){
          if(index.set_id.set_id == 20){
            this.deterRunes = this.deterRunes + 1;
          }
          if(index.set_id.set_id == 19){
            this.fightRunes = this.fightRunes + 1;
          }
        }
        if(this.deterRunes == 2){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 1;
        }
        if(this.deterRunes == 4){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 2;
        }
        if(this.deterRunes == 6){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 3;
        }
        if(this.fightRunes == 2){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 1;
        }
        if(this.fightRunes == 4){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 2;
        }
        if(this.fightRunes == 6){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 3;
        }
      } else if(index.unit_master_id.name === "Icaru" && index.unit_id !== this.icaruId && index.unit_id !== this.secondIcaruId && this.thirdIcaruId == undefined){
        this.fightRunes = 0;
        this.deterRunes = 0;
        this.thirdIcaru = index;
        this.hpThird =  index.unit_master_id.hp.toFixed() - this.thirdIcaru.unit_master_id.base_hp.toFixed();
        this.defThird = this.thirdIcaru.def.toFixed() - this.thirdIcaru.unit_master_id.base_def.toFixed();
        this.spdThird = this.thirdIcaru.spd.toFixed() - this.thirdIcaru.unit_master_id.base_spd.toFixed();
        this.atkThird = this.thirdIcaru.atk.toFixed() - this.thirdIcaru.unit_master_id.base_atk.toFixed();
        this.crit_rateThird = this.thirdIcaru.critical_rate.toFixed() - this.thirdIcaru.unit_master_id.base_crit_rate.toFixed();
        this.crit_damageThird = this.thirdIcaru.critical_damage.toFixed() - this.thirdIcaru.unit_master_id.base_crit_damage.toFixed();
        this.totalThirdIcaruSpd = 108 * (100 + this.spdBuilding + 28)/ 100 + this.spdThird;
        this.thirdIcaruId = this.thirdIcaru.unit_id;
        for(let index of this.thirdIcaru.runes){
          if(index.set_id.set_id == 20){
            this.deterRunes = this.deterRunes + 1;
          }
          if(index.set_id.set_id == 19){
            this.fightRunes = this.fightRunes + 1;
          }
        }
        if(this.deterRunes == 2){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 1;
        }
        if(this.deterRunes == 4){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 2;
        }
        if(this.deterRunes == 6){
          this.tricaruDeterSetCount = this.tricaruDeterSetCount + 3;
        }
        if(this.fightRunes == 2){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 1;
        }
        if(this.fightRunes == 4){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 2;
        }
        if(this.fightRunes == 6){
          this.tricaruFightSetCount = this.tricaruFightSetCount + 3;
        }
      }
    }
    this.onChange(event);
  }
  onSelect(rune): void {
    this.selectedRunes = rune;
  }
  onVerdeSelect(verde): void {
    this.selectedVerde = verde;
    this.verdehileDisplay();
    this.showSelectVerde = true;
    localStorage.setItem("verde_select", JSON.stringify(this.selectedVerde));
  }
  showSelect(){
    this.selectedVerde = 0;
    localStorage.removeItem('verde_select');
    this.showSelectVerde = !this.showSelectVerde;
  }
}
