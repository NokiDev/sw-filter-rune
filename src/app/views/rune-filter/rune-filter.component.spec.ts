import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RuneFilterComponent } from './rune-filter.component';

describe('RuneFilterComponent', () => {
  let component: RuneFilterComponent;
  let fixture: ComponentFixture<RuneFilterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RuneFilterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RuneFilterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
