/* Angular*/
import { Component, ViewChild } from "@angular/core";
import { SelectionModel } from "@angular/cdk/collections";
/* Angular Material */
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import { FormControl } from '@angular/forms';


import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
/* FontAwesome */
import { faStar, faCartPlus } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-rune-filter",
  templateUrl: "./rune-filter.component.html",
  styleUrls: ["../../../styles.css"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class RuneFilterComponent {
  isLoading = false;
  /* FontAwesome Icons */
  cartPlus = faCartPlus;
  star = faStar;
  /* Data */
  data: any = [];
  addCard: any = [];
  card: any = [];
  /* Rune Sumup */
  finalSumUp: any;
  sumUp = 0;
  /* Table */
  dataSource: any;

  myUnitRunes: any = [];
  totalRunes: any = [];
  filterTotalRunes: any = [];

  isSpeedCheck: boolean = false;

  displayedColumns: string[] = [
    "rune_set",
    "extra.unit_name",
    "set_id.set_name",
    "grade",
    "upgrade_curr",
    "rune_main_stat",
    "innate_stat",
    "atkPercent",
    "atkFlat",
    "defPercent",
    "defFlat",
    "hpPercent",
    "hpFlat",
    "spd",
    "accuracy",
    "resistance",
    "critRate",
    "critDdmg",
    "sell_value",
  ];

  selection = new SelectionModel<any>(true, []);
  expandedElement: any;
  /* Table Unequiped Rune */
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild("matSortTotal", { static: true }) matSortTotal: MatSort;
  @ViewChild("matTablePaginatorStorage", { static: true })
  matTablePaginatorStorage: MatPaginator;

  constructor() {}
  
  ngOnInit() {
    this.data = JSON.parse(localStorage.getItem("wizard_data"));
    for (let index in this.data.unit_list) {
      for (let j in this.data.unit_list[index].runes) {
        this.myUnitRunes.push(this.data.unit_list[index].runes[j]);
      }
    }
    this.totalRunes = this.data.runes.concat(this.myUnitRunes);
    /* Define Datasource for table */
    this.dataSource = new MatTableDataSource(this.totalRunes);
    this.expandedElement = this.data.unit_list;

    /* Sort filter for nested object */
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        case "set_id.set_name":
          return item.set_id.set_name;
        case "rune_main_stat":
          return item.pri_eff.pri_eff;
        case "innate_stat":
          return item.prefix_eff.type;
        case "rune_set":
          return  item.slot_no.id;
        case "atkPercent": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff === 'ATK%'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "atkFlat": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff === 'ATK'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "defPercent": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff === 'DEF%'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "defFlat": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff == 'DEF'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "hpPercent": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff == 'HP%'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "hpFlat": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff == 'HP'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "spd": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff == 'SPD'){  
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "accuracy": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff == 'ACC'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "resistance": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff == 'RES'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "critRate": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff == 'CRate'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        case "critDdmg": 
          for(let sec_eff in item.sec_eff){
            if(item.sec_eff[sec_eff].sec_eff == 'CDmg'){
              return item.sec_eff[sec_eff].sec_eff_total;
            }
          }
          break;
        default:
          return item[property];
      }
    };

    this.dataSource.filterPredicate = function (
      data,
      filter: string,
    ): boolean {
        return (
          data.set_id.set_name.toString().toLowerCase().includes(filter) ||
          data.pri_eff.pri_eff.toString().toLowerCase().includes(filter) || 
          data.extra.unit_name?.toString().toLowerCase().includes(filter)
        );
    };
    this.dataSource.paginator = this.matTablePaginatorStorage;
    this.dataSource.sort = this.matSortTotal;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
    this.dataSource.filterSub;
  }
  
}
