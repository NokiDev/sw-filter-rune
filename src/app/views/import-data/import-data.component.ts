/* Angular*/
import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import {Router} from '@angular/router';
/* FontAwesome Icons */
import { faCheckCircle, faTimesCircle, faFileImport, faFolder, faTrash } from "@fortawesome/free-solid-svg-icons";
/* Angular Material */
import { MatDialog } from '@angular/material/dialog';
/* Component */
import { RuneImport } from '../../core/mapping/rune-map.component';
import { ArtifactImport } from '../../core/mapping/artifact-map.component';
import { BuildingImport } from '../../core/mapping/building-map.component';

/* File State */
export enum ImportState {
  none,
  importing,
  imported,
  loaded,
}

@Component({
  selector: "app-import-data",
  templateUrl: "./import-data.component.html",
  styleUrls: ["../../../styles.css"],
})
export class ImportDataComponent implements OnInit {
  showSide = false;
  /* Set Import State */
  ImportState = ImportState; 
  importState = ImportState.none;
  selectedFile: File;
  /* FontAwesome Icons */
  checkCircle = faCheckCircle;
  timeCircle = faTimesCircle;
  file = faFileImport;
  folder = faFolder;
  trash = faTrash;
  /* Data */
  data: any = [];
  building: any = [];
  monsterArtifact: any = [];
  rune: any = [];
  artifact: any = [];
  buildings: any = [];
  monsters: any = [];
  artifacts: any = [];
  value: number;
  /* Sets Count */
  hpSetCount = 0;
  guardSetCount = 0;
  swiftSetCount = 0;
  bladeSetCount = 0;
  rageSetCount = 0;
  focusSetCount = 0;
  endureSetCount = 0;
  fatalSetCount = 0;

  checked: boolean = false;

  constructor(private http: HttpClient, public dialog: MatDialog, private route:Router) {}

  ngOnInit() {
    this.checked = JSON.parse(localStorage.getItem("isChecked"));
    /* Wizard Rune/Artifact/Buildings Import */
    let rune = new RuneImport();
    let artifact = new ArtifactImport();
    let buildings = new BuildingImport();

    this.rune = rune.runeImport;
    this.buildings = buildings.buildingImport;
    this.artifact = artifact.artifactImport;

    this.data = JSON.parse(localStorage.getItem("wizard_data"));
    this.building = JSON.parse(localStorage.getItem("wizard_building"));
  }

  async importJson() {
    this.importState = ImportState.importing;
    this.importArtifact();
    await this.monster();
    await this.importBuilding();
    this.importState = ImportState.imported;
    this.dialog.closeAll();
    this.route.navigate(['/monsters']);
  }

  openDialog() {
    this.dialog.open(loadingDialog, { disableClose: true });
  }

  /* CALL API FOR ALL MONSTERS  */
  async monster() {
    let monsters = JSON.parse(localStorage.getItem("monster_data"));

    if (monsters != null && monsters.length != 0) {
      this.importMonster({ results: monsters });
    } else {
      monsters = [];
      let apiCallPromises = [];
      /* TODO : Fix Hardcoded page count */
      for (let i = 1; i < 19; i++) {
        /* TODO : Fix API Call method */
        let nextPage = "http://sw.kowoshiria.me/api/v2/monsters/?page=" + i;
        let promise = this.http
          .get<any>(nextPage)
          .toPromise();
        apiCallPromises.push(promise);
      }
      let result = (await Promise.all(apiCallPromises)).map( x => x.results).flat();
      this.importMonster({ results: result });
    }
    this.getStorageRunes();

    localStorage.setItem("wizard_data", JSON.stringify(this.data));
    localStorage.setItem("monster_data", JSON.stringify(monsters));
  }

  private handleError<T>(operation = "operation", result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    };
  }

  /* Map Buildings */
  async importBuilding() {
    let buildings = new BuildingImport();
    this.buildings = buildings.buildingImport;
    
    for (let index in this.data.building_list){
      const buildingList = this.data.building_list[index];
      buildingList.building_master_id = {
        id: buildingList.building_master_id,
        name: this.buildings.name[buildingList.building_master_id],
        img: this.buildings.img[buildingList.building_master_id],
        level: 0,
      }
    }
    for (let index in this.data.deco_list){
      const decoList = this.data.deco_list[index];

      decoList.master_id = {
        id: decoList.master_id,
        name: this.buildings.name[decoList.master_id],
        img: this.buildings.img[decoList.master_id]
      }

      if(!decoList.master_id.name){
        decoList.lvl = {
          lvl: 0,
          stat: 0,
          img: this.buildings.img[decoList.master_id]
        }
      }
      if(decoList.master_id.name == "Sky Tribe Totem"){
        decoList.lvl = {
          lvl: decoList.level,
          Speed: this.buildings.levelSpeed[decoList.level],
          type: "Speed"
        }
      }
      if(decoList.master_id.name == "Fire Sanctuary"){
        decoList.lvl = {
          lvl: decoList.level,
          FireAttack: this.buildings.levelAtkFire[decoList.level],
          type: "Fire Atk"
        }
      }
      if(decoList.master_id.name == "Fallen Ancient Guardian"){
        decoList.lvl = {
          lvl: decoList.level,
          CritDamage: this.buildings.levelCritDamage[decoList.level],
          type: "C.Dmg"
        }
      }
      if(decoList.master_id.name == "Ancient Sword"){
        decoList.lvl = {
          lvl: decoList.level,
          Attack: this.buildings.levelAtk[decoList.level],
          type: "Atk"
        }
      }
      if(decoList.master_id.name == "Guardstone"){
        decoList.lvl = {
          lvl: decoList.level,
          Def: this.buildings.guardStone[decoList.level],
          type: "Def"
        }
      }
      if(decoList.master_id.name == "Crystal Altar"){
        decoList.lvl = {
          lvl: decoList.level,
          Hp: this.buildings.crystalAltar[decoList.level],
          type: "Hp"
        }
      }
      if(decoList.master_id.name == "Water Sanctuary"){
        decoList.lvl = {
          lvl: decoList.level,
          WaterAttack: this.buildings.levelAtkWater[decoList.level],
          type: "Water Atk"
        }
      }
      if(decoList.master_id.name == "Wind Sanctuary"){
        decoList.lvl = {
          lvl: decoList.level,
          WindAttack: this.buildings.levelAtkWind[decoList.level],
          type: "Wind Atk"
        }
      }
    }
    localStorage.setItem("wizard_building", JSON.stringify(this.data.deco_list));
  }

  /* Map Storage Runes */
  async getStorageRunes() {
    for (let index in this.data.runes) {
      let runes_list = this.data.runes[index];

      runes_list.slot_no = {
        id: runes_list.slot_no,
        rune_img: this.rune.rune_img[runes_list.slot_no],
      };
      runes_list.extra = {
        id: runes_list.extra,
        name: this.rune.extra[runes_list.extra],
      };
      runes_list.prefix_eff = {
        type: this.rune.effectTypes[runes_list.prefix_eff[0]],
        value: runes_list.prefix_eff[1],
      };
      runes_list.set_id = {
        set_id: runes_list.set_id,
        set_name: this.rune.sets[runes_list.set_id],
        set_img: this.rune.sets_img[runes_list.set_id],
      };
      /* RUNES MAIN EFEFCT */
      runes_list.pri_eff = {
        pri_eff_id: runes_list.pri_eff[0],
        pri_eff: this.rune.effectTypes[runes_list.pri_eff[0]],
        pri_eff_value: runes_list.pri_eff[1],
      };
      /* RUNES SECONDARY EFEFCT */
      for (let j in runes_list.sec_eff) {
        runes_list.sec_eff[j] = {
          sec_eff_id: runes_list.sec_eff[j][0],
          sec_eff: this.rune.effectTypes[runes_list.sec_eff[j][0]],
          sec_eff_value: runes_list.sec_eff[j][1],
          sec_eff_gems: runes_list.sec_eff[j][2],
          sec_eff_grindstones: runes_list.sec_eff[j][3],
          sec_eff_total: runes_list.sec_eff[j][1] + runes_list.sec_eff[j][3]
        };
      }
    }
  }

  /* Map Monsters & Equiped Runes */
  async importMonster(data: any) {
    let result: any;
    for (result in data.results) {
      let unit = data.results[result];

      for (let index in this.data.unit_list) {
        if (this.data.unit_list[index].unit_master_id === unit.com2us_id) {
          const unit_list = this.data.unit_list[index];

          /* Monster Mapping */
          unit_list.unit_master_id = {
            id: unit_list.unit_master_id,
            name: unit.name,
            hp: unit.max_lvl_hp,
            base_hp: unit.max_lvl_hp,
            element: unit.element,
            image: unit.image_filename,
            base_atk: unit_list.atk,
            base_def: unit_list.def,
            base_crit_rate: unit_list.critical_rate,
            base_crit_damage: unit_list.critical_damage,
            base_res: unit_list.resist,
            base_accuracy: unit_list.accuracy,
            base_spd: unit_list.spd,
            principal_set: "",
            secondary_set: "",
          };
          /* Rune Mapping */
          for (let i in unit_list.runes) {
            const master_id = this.data.unit_list[index].unit_master_id;
            let runes_list = unit_list.runes[i];

            /* Rune Class */
            if(runes_list.class == 16){
              runes_list.class = 6;
            }
            if(runes_list.class == 15){
              runes_list.class = 5;
            }
            /* Rune Slot Number */
            runes_list.slot_no = {
              id: runes_list.slot_no,
              rune_img: this.rune.rune_img[runes_list.slot_no],
            };
            /* Rune Rarity */
            runes_list.extra = {
              id: runes_list.extra,
              name: this.rune.extra[runes_list.extra],
              unit_img: unit.image_filename,
              unit_name: unit.name,
              rune_bg: this.rune.background[runes_list.rank]
            };
            /* Rune Innate Stats */
            runes_list.prefix_eff = {
              type: this.rune.effectTypes[runes_list.prefix_eff[0]],
              value: runes_list.prefix_eff[1],
            };
            /* Rune Set */
            runes_list.set_id = {
              set_id: runes_list.set_id,
              set_name: this.rune.sets[runes_list.set_id],
              set_img: this.rune.sets_img[runes_list.set_id],
            };
            /* Rune Main Effect */
            runes_list.pri_eff = {
              pri_eff_id: runes_list.pri_eff[0],
              pri_eff: this.rune.effectTypes[runes_list.pri_eff[0]],
              pri_eff_value: runes_list.pri_eff[1],
            };
            /* Rune Sets Mapping */
            /* Energy */
            if(unit_list.runes[i].set_id.set_id == 1) {
              this.hpSetCount = this.hpSetCount + 1;
              if(this.hpSetCount == 2 || this.hpSetCount == 4 || this.hpSetCount == 6){
                master_id.hp = master_id.hp + (master_id.base_hp * 15 / 100);
              }
            }
            /* Guard */
            if(unit_list.runes[i].set_id.set_id == 2) {
              this.guardSetCount = this.guardSetCount + 1;
              if(this.guardSetCount == 2 || this.guardSetCount == 4 || this.guardSetCount == 6){
                unit_list.def = unit_list.def + (master_id.base_def * 15 / 100);
              }
            }
            /* Swift */
            if(unit_list.runes[i].set_id.set_id == 3) {
              this.swiftSetCount = this.swiftSetCount + 1;
              if(this.swiftSetCount == 4){
                unit_list.spd = unit_list.spd + (master_id.base_spd * 25 / 100);
              }
            }
            /* Blade */
            if(unit_list.runes[i].set_id.set_id == 4) {
              this.bladeSetCount = this.bladeSetCount + 1;
              if(this.bladeSetCount == 2 || this.bladeSetCount == 4 || this.bladeSetCount == 6){
                unit_list.critical_rate = unit_list.critical_rate + 12;
              }
            }
            /* Rage */
            if(unit_list.runes[i].set_id.set_id == 5) {
              this.rageSetCount = this.rageSetCount + 1;
              if(this.rageSetCount == 4){
                unit_list.critical_damage = unit_list.critical_damage + 40;
              }
            }
            /* Focus */
            if(unit_list.runes[i].set_id.set_id == 6) {
              this.focusSetCount = this.focusSetCount + 1;
              if(this.focusSetCount == 2 || this.focusSetCount == 4 || this.focusSetCount == 6){
                unit_list.accuracy = unit_list.accuracy + 20;
              }
            }
            /* Endure */
            if(unit_list.runes[i].set_id.set_id == 7) {
              this.endureSetCount = this.endureSetCount + 1;
              if(this.endureSetCount == 2 || this.endureSetCount == 4 || this.endureSetCount == 6){
                unit_list.resist = unit_list.resist + 20;
              }
            }
            /* Fatal */
            if(unit_list.runes[i].set_id.set_id == 8) {
              this.fatalSetCount = this.fatalSetCount + 1;
              if(this.fatalSetCount == 4){
                unit_list.atk = unit_list.atk + (master_id.base_atk * 35 / 100);;
              }
            }
            /* Rune Secondary Effect Parse */
            const eff_value = this.data.unit_list[index].runes[i].pri_eff.pri_eff_value;
            
            /* Rune Secondary Effect type */
            if (unit_list.runes[i].pri_eff.pri_eff_id == 1) {
              master_id.hp = master_id.hp + eff_value;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 2) {
              master_id.hp = master_id.hp + (master_id.base_hp * eff_value) / 100;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 3) {
              unit_list.atk = unit_list.atk + eff_value;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 4) {
              unit_list.atk = unit_list.atk + (master_id.base_atk * eff_value) / 100;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 5) {
              unit_list.def = unit_list.def + eff_value;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 6) {
              unit_list.def = unit_list.def + (master_id.base_def * eff_value) / 100;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 8) {
              unit_list.spd = unit_list.spd + eff_value;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 9) {
              unit_list.critical_rate = unit_list.critical_rate + eff_value;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 10) {
              unit_list.critical_damage = unit_list.critical_damage + eff_value;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 11) {
              unit_list.resist = unit_list.resist + eff_value;
            }
            if (unit_list.runes[i].pri_eff.pri_eff_id == 12) {
              unit_list.accuracy = unit_list.accuracy + eff_value;
            }

            for (let j in unit_list.runes[i].sec_eff) {
              unit_list.runes[i].sec_eff[j] = {
                sec_eff_id: unit_list.runes[i].sec_eff[j][0],
                sec_eff: this.rune.effectTypes[
                  unit_list.runes[i].sec_eff[j][0]
                ],
                sec_eff_value: unit_list.runes[i].sec_eff[j][1],
                sec_eff_gems: unit_list.runes[i].sec_eff[j][2],
                sec_eff_grindstones: unit_list.runes[i].sec_eff[j][3],
                sec_eff_total: unit_list.runes[i].sec_eff[j][1] + unit_list.runes[i].sec_eff[j][3]
              };
              const sec_eff_value = unit_list.runes[i].sec_eff[j].sec_eff_value;
              const sec_eff_grinstones = unit_list.runes[i].sec_eff[j].sec_eff_grindstones;

              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 1) {
                master_id.hp = master_id.hp + sec_eff_value + sec_eff_grinstones;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 2) {
                master_id.hp = master_id.hp +
                  (master_id.base_hp * (sec_eff_value + sec_eff_grinstones)) / 100;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 3) {
                unit_list.atk = unit_list.atk + sec_eff_value + sec_eff_grinstones;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 4) {
                unit_list.atk = unit_list.atk +
                  (master_id.base_atk * (sec_eff_value + sec_eff_grinstones)) / 100;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 5) {
                unit_list.def =
                  unit_list.def + sec_eff_value + sec_eff_grinstones;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 6) {
                unit_list.def =
                  unit_list.def +
                  (master_id.base_def * (sec_eff_value + sec_eff_grinstones)) / 100;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 8) {
                unit_list.spd =
                  unit_list.spd + sec_eff_value + sec_eff_grinstones;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 9) {
                unit_list.critical_rate =
                  unit_list.critical_rate + sec_eff_value + sec_eff_grinstones;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 10) {
                unit_list.critical_damage =
                  unit_list.critical_damage +
                  sec_eff_value +
                  sec_eff_grinstones;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 11) {
                unit_list.resist =
                  unit_list.resist + sec_eff_value + sec_eff_grinstones;
              }
              if (unit_list.runes[i].sec_eff[j].sec_eff_id == 12) {
                unit_list.accuracy =
                  unit_list.accuracy + sec_eff_value + sec_eff_grinstones;
              }
            }
            for(let j in unit_list.runes[i].prefix_eff){
              if (unit_list.runes[i].prefix_eff.type == "CRate") {
                unit_list.critical_rate = unit_list.critical_rate + (unit_list.runes[i].prefix_eff.value / 2);
              }
              if (unit_list.runes[i].prefix_eff.type == "CDmg") {
                unit_list.critical_damage = unit_list.critical_damage + (unit_list.runes[i].prefix_eff.value / 2);
              }
              if (unit_list.runes[i].prefix_eff.type == "DEF") {
                unit_list.def = unit_list.def + (unit_list.runes[i].prefix_eff.value / 2);
              }
              if (unit_list.runes[i].prefix_eff.type == "DEF%") {
                unit_list.def = unit_list.def + (unit_list.runes[i].prefix_eff.value * unit_list.unit_master_id.base_def / 100 );
              }
              if (unit_list.runes[i].prefix_eff.type == "HP") {
                master_id.hp = master_id.hp + (unit_list.runes[i].prefix_eff.value / 2);
              }
              if (unit_list.runes[i].prefix_eff.type == "HP%") {
                master_id.hp = master_id.hp + (unit_list.runes[i].prefix_eff.value * unit_list.unit_master_id.base_hp / 100 );
              }
              if (unit_list.runes[i].prefix_eff.type == "SPD") {
                unit_list.spd = unit_list.spd + (unit_list.runes[i].prefix_eff.value / 2);
              }
            }
          }
          const master_id = this.data.unit_list[index].unit_master_id;
          for(let i in unit_list.artifacts){
            if (unit_list.artifacts[i].pri_effect.main == 'HP'){
              master_id.hp = master_id.hp + unit_list.artifacts[i].pri_effect.value;
            }
            if (unit_list.artifacts[i].pri_effect.main == 'ATK'){
              unit_list.atk = unit_list.atk + unit_list.artifacts[i].pri_effect.value;
            }
            if (unit_list.artifacts[i].pri_effect.main == 'DEF'){
              unit_list.def = unit_list.def + unit_list.artifacts[i].pri_effect.value;
            }
          }
        }
        this.guardSetCount = 0;
        this.hpSetCount = 0;
        this.guardSetCount = 0;
        this.swiftSetCount = 0;
        this.bladeSetCount = 0;
        this.rageSetCount = 0;
        this.focusSetCount = 0;
        this.endureSetCount = 0;
        this.fatalSetCount = 0;
      }
    }
  }

  /* Map Artifacts */
  async importArtifact(){
    const monsters = this.data.unit_list;
    
    for(let monster in monsters){
      const artifacts = monsters[monster].artifacts;
      for(let artifact in artifacts){
        /* Type Mapping */
        if(artifacts[artifact].slot === 1){
          artifacts[artifact].attribute = {
            element: this.artifact.effectTypes.attribute[artifacts[artifact].attribute],
            value: artifacts[artifact].attribute,
            elementImg: this.artifact.effectTypes.elementImg[artifacts[artifact].attribute]
          }
        }
        if(artifacts[artifact].slot === 2){
          artifacts[artifact].unit_style = {
            archetype: this.artifact.effectTypes.archetype[artifacts[artifact].unit_style],
            value: artifacts[artifact].unit_style,
            archetypeImg: this.artifact.effectTypes.archetypeImg[artifacts[artifact].unit_style]
          }
        }
        /* Level / Main / Secondary Stats */
        artifacts[artifact].rank = {
          naturalRank: this.artifact.effectTypes.rank[artifacts[artifact].natural_rank],
          rank: this.artifact.effectTypes.rank[artifacts[artifact].rank],
        }
        artifacts[artifact].pri_effect = {
          main: this.artifact.effectTypes.main[artifacts[artifact].pri_effect[0]],
          value: artifacts[artifact].pri_effect[1],
          level: artifacts[artifact].pri_effect[2]
        }
        
        const secEffects = monsters[monster].artifacts[artifact].sec_effects;
        for(let secEffect in secEffects){
          artifacts[artifact].sec_effects[secEffect] = {
            type: artifacts[artifact].sec_effects[secEffect][0],
            value: artifacts[artifact].sec_effects[secEffect][1]
          } 
          this.value = artifacts[artifact].sec_effects[secEffect].value;
          this.artifact.effectTypes.sub = {
            200: `ATK Increased Proportional to Lost HP up to ${this.value}%`,
            201: `DEF Increased Proportional to Lost HP up to ${this.value}%`,
            202: `SPD Increased Proportional to Lost HP up to ${this.value}%`,
            203: `SPD Under Inability Effects +${this.value}%`,
            204: `ATK Increasing Effect +${this.value}%`,
            205: `DEF Increasing Effect +${this.value}%`,
            206: `SPD Increasing Effect +${this.value}%`,
            207: `Crit Rate Increasing Effect +${this.value}%`,
            208: `Damage Dealt by Counterattack +${this.value}%`,
            209: `Damage Dealt by Attacking Together +${this.value}%`,
            210: `Bomb Damage +${this.value}%`,
            211: `Damage Dealt by Reflected DMG +${this.value}%`,
            212: `Crushing Hit DMG +${this.value}%`,
            213: `Damage Received Under Inability Effect -${this.value}%`,
            214: `Received Crit DMG -${this.value}%`,
            215: `Life Drain +${this.value}%`,
            216: `HP when Revived +${this.value}%`,
            217: `Attack Bar when Revived +${this.value}%`,
            218: `Additional Damage by ${this.value}% of HP`,
            219: `Additional Damage by ${this.value}% of ATK`,
            220: `Additional Damage by ${this.value}% of DEF`,
            221: `Additional Damage by ${this.value}% of SPD`,
            222: `CRIT DMG+ up to ${this.value}% as the enemy's HP condition is good`,
            223: `CRIT DMG+ up to ${this.value}% as the enemy's HP condition is bad`,
            224: `Single-target skill CRIT DMG ${this.value}% on your turn`,
            300: `Damage Dealt on Fire +${this.value}%`,
            301: `Damage Dealt on Water +${this.value}%`,
            302: `Damage Dealt on Wind +${this.value}%`,
            303: `Damage Dealt on Light +${this.value}%`,
            304: `Damage Dealt on Dark +${this.value}%`,
            305: `Damage Received from Fire -${this.value}%`,
            306: `Damage Received from Water -${this.value}%`,
            307: `Damage Received from Wind -${this.value}%`,
            308: `Damage Received from Light -${this.value}%`,
            309: `Damage Received from Dark -${this.value}%`,
            400: `Skill 1 CRIT DMG +${this.value}%`,
            401: `Skill 2 CRIT DMG +${this.value}%`,
            402: `Skill 3 CRIT DMG +${this.value}%`,
            403: `Skill 4 CRIT DMG +${this.value}%`,
            404: `Skill 1 Recovery +${this.value}%`,
            405: `Skill 2 Recovery +${this.value}%`,
            406: `Skill 3 Recovery +${this.value}%`,
            407: `Skill 1 Accuracy +${this.value}%`,
            408: `Skill 2 Accuracy +${this.value}%`,
            409: `Skill 3 Accuracy +${this.value}%`,
          }
          artifacts[artifact].sec_effects[secEffect] = {
            main: this.artifact.effectTypes.sub[artifacts[artifact].sec_effects[secEffect].type],
            value: artifacts[artifact].sec_effects[secEffect].value,
            id: artifacts[artifact].sec_effects[secEffect].type,
          } 
        }
      }
    }
    localStorage.setItem("monster_artifacts", JSON.stringify(this.data.unit_list));
    localStorage.removeItem('verde_select');
    localStorage.removeItem('lushen_select');
  }

  onFileLoad(event) {
    this.importState = ImportState.none;
    this.selectedFile = event.target.files[0];
    const fileReader = new FileReader();
    fileReader.readAsText(this.selectedFile, "UTF-8");
    fileReader.onload = () => {
      this.data = JSON.parse(fileReader.result as string);
      this.importState = ImportState.loaded;
    };
  }

  onChange(checked: boolean) {
    this.checked = checked;
    localStorage.setItem("isChecked", JSON.stringify(this.checked));
    window.location.reload();
  }

  clearData(){
    localStorage.removeItem('wizard_data');
    localStorage.removeItem('monster_data');
    localStorage.removeItem('wizard_building');
    localStorage.removeItem('isChecked');
    localStorage.removeItem('monster_artifafcts');
    localStorage.removeItem('lushen_select');
    localStorage.removeItem('verde_select');
    window.location.reload();
  }
}

@Component({
  selector: 'loadingDialog',
  templateUrl: 'loadingDialog.html',
  styleUrls: ["./loading-dialog.scss"],
})
export class loadingDialog {}
