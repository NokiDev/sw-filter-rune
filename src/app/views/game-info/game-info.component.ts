/* Angular*/
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-game-info',
  templateUrl: './game-info.component.html',
  styleUrls: ['./game-info.component.scss']
})
export class GameInfoComponent implements OnInit {

  panelOpenState: boolean;

  constructor() { }

  ngOnInit(): void {
    this.panelOpenState = true;
  }
}
