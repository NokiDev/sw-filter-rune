/* Angular*/
import { Component, OnInit } from '@angular/core';
/* FontAwesome Icons */
import { faCheckCircle, faTimesCircle, faShieldAlt, faRunning, faStar, faHeart, faBahai, faPlusCircle, faEdit } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-bj5',
  templateUrl: './bj5.component.html',
  styleUrls: ['./bj5.component.scss']
})
export class Bj5Component implements OnInit {
  /* Data */
  data: any = [];
  building: any = [];
  baleygr: any = [];
  /* FontAwesome Icons */
  star = faStar;
  checkCircle = faCheckCircle;
  timeCircle = faTimesCircle;
  shield = faShieldAlt;
  run = faRunning;
  heart = faHeart;
  bahai = faBahai;
  plusCircle = faPlusCircle;
  edit = faEdit;
  /* Baleygr Ehp & Dmg check */
  validMinHit: boolean;
  validMaxHit: boolean;
  validAverageHit: boolean;
  validEhp: boolean;
  
  hp: number;
  eHp: number;
  def: number;
  spd: number;
  atk: number;
  crit_damage: number;
  crit_rate: number;
  /* Define Base Building Value */
  defBuilding = 0;
  atkBuilding = 0;
  critDamageBuilding = 0;
  fireAtkBuilding = 0;
  spdBuilding = 0;
  hpBuilding = 0;
  /* Bonus Stats */
  fightSet: number;
  guildSkill: number;
  atkLeader: number;
  atkPercentBonus: number;
  /* FrontLine Requirement EhP */
  frontLine: number;
  requiredEhP: number;
  /* Baleygr Stats Max/Min Damage For last hit */
  baleygrAtk: number;
  baleygrBoostedAtk: number;
  maxHit: number;
  minHit: number;
  averageHit: number;
  /* Skill Bonus */
  skillCrit: number;
  skillUp: number;
  /* Artifacts Bonus Damage */
  dmgBonusPerAtk: number;
  dmgBonusPerHp: number;
  dmgBonusPerDef: number;
  dmgBonusPerSpeed: number;
  
  isAtkBuilding = false;
  isHpBuilding = false;
  isDefBuilding = false;
  isCritBuilding = false;
  isFireAtkBuilding = false;
  isSpeedBuilding = false;

  selectSpdBuilding = 0;
  selectFireBuilding = 0;
  selectAttackBuilding = 0;
  selectDefBuilding = 0;
  selectHpBuilding = 0;
  selectCritBuilding = 0;

  selectedRunes: any;
  /* Building Base Select Input */
  itemSpdBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 3},
    { id: 3, value: 5},
    { id: 4, value: 6},
    { id: 5, value: 8},
    { id: 6, value: 9},
    { id: 7, value: 11},
    { id: 8, value: 12},
    { id: 9, value: 14},
    { id: 10, value: 15},
  ];
  itemFireBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 3},
    { id: 2, value: 5},
    { id: 3, value: 7},
    { id: 4, value: 9},
    { id: 5, value: 11},
    { id: 6, value: 13},
    { id: 7, value: 15},
    { id: 8, value: 17},
    { id: 9, value: 19},
    { id: 10, value: 21},
  ];
  itemAttackBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 4},
    { id: 3, value: 6},
    { id: 4, value: 8},
    { id: 5, value: 10},
    { id: 6, value: 12},
    { id: 7, value: 14},
    { id: 8, value: 16},
    { id: 9, value: 18},
    { id: 10, value: 20},
  ];
  itemCritBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 5},
    { id: 3, value: 7},
    { id: 4, value: 10},
    { id: 5, value: 12},
    { id: 6, value: 15},
    { id: 7, value: 17},
    { id: 8, value: 20},
    { id: 9, value: 22},
    { id: 10, value: 25},
  ];
  itemDefBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 4},
    { id: 3, value: 6},
    { id: 4, value: 8},
    { id: 5, value: 10},
    { id: 6, value: 12},
    { id: 7, value: 14},
    { id: 8, value: 16},
    { id: 9, value: 18},
    { id: 10, value: 20},
  ];
  itemHpBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 4},
    { id: 3, value: 6},
    { id: 4, value: 8},
    { id: 5, value: 10},
    { id: 6, value: 12},
    { id: 7, value: 14},
    { id: 8, value: 16},
    { id: 9, value: 18},
    { id: 10, value: 20},
  ];

  constructor() { }

  ngOnInit(): void {
    this.data = JSON.parse(localStorage.getItem("wizard_data"));
    this.building = JSON.parse(localStorage.getItem("wizard_building"));

    if(this.data){
      for(let i of this.data.unit_list){
        if(i.unit_master_id.id == 22612){
          this.hp = i.unit_master_id.hp - 10215;
        }
      }
    } 
    this.buildingDisplay();

    this.hp = 0;
    this.def = 0;
    this.spd = 0;
    this.atk = 0;
    this.crit_rate = 0;
    this.crit_damage = 0;
    
    this.frontLine = 2;
    this.fightSet = 12;
    this.guildSkill = 5;
    this.atkLeader = 40;
    this.atkPercentBonus = 0;
    this.skillCrit = 0;
    this.skillUp = 30;

    this.dmgBonusPerAtk = 0;
    this.dmgBonusPerHp = 0;
    this.dmgBonusPerDef = 0;
    this.dmgBonusPerSpeed = 0;
    
    this.baleygrDisplay();
    this.onChange(event);
  }
  onChange(event) {
    /* Baleygr Max/Min hit Formula */
    this.baleygrAtk = 790 * (1 + ((this.fightSet * 8 ) + this.atkLeader + this.selectAttackBuilding + this.selectFireBuilding + this.guildSkill)/100) + this.atk;
    this.baleygrBoostedAtk = this.baleygrAtk * (1.5+( this.atkPercentBonus/100)*0.5);

    let atkBonus = 1.5 + (this.atkPercentBonus/100) * 0.5;
    let baleygrHpBonus = 10215 * (1 + (this.selectHpBuilding + this.guildSkill)/100) + this.hp;
    let baleygrDefBonus = 670 * (1 + (this.selectDefBuilding + this.guildSkill)/100) + this.def;
    let baleygrSpdBonus = 102 * (1 + (this.selectSpdBuilding + this.guildSkill)/100) + this.spd;
    let baleygrAtkBonus = this.baleygrBoostedAtk * atkBonus * (this.dmgBonusPerAtk/100);
  
    this.maxHit = (((this.baleygrAtk * (1.5+( this.atkPercentBonus / 100)*0.5)) * 4 * (1 + (this.crit_damage + 50 + this.skillCrit + this.skillUp + this.selectCritBuilding)/100))*(1000/(1140 + 3.5 * 801))) * 1.4 + 
    baleygrAtkBonus + (baleygrDefBonus * (this.dmgBonusPerDef / 100)) + (baleygrHpBonus * (this.dmgBonusPerHp / 100)) + (baleygrSpdBonus * (this.dmgBonusPerSpeed / 100));

    this.minHit = this.maxHit * 0.95;
    this.averageHit = (this.minHit + this.maxHit) / 2;
    /* Baleygr Ehp Formula */
    this.eHp = ((10215 + this.hp + (10215 * (this.guildSkill + this.selectHpBuilding) /100))*(1140 + (670 + this.def + (670 * (this.guildSkill + this.selectDefBuilding) /100)) * 3.5)/1000);
    
    if(this.frontLine == 2){
      this.requiredEhP = 92000;
    }
    if(this.frontLine == 3){
      this.requiredEhP = 85000;
    }
    if(this.frontLine == 4){
      this.requiredEhP = 78000;
    }

    if(this.minHit >= 32000){
      this.validMinHit = true;
    } else {
      this.validMinHit = false;
    }

    if(this.maxHit >= 32000){
      this.validMaxHit = true;
    } else {
      this.validMaxHit = false;
    }

    if(this.averageHit >= 32000){
      this.validAverageHit = true;
    } else {
      this.validAverageHit = false;
    }

    if(this.eHp > this.requiredEhP){
      this.validEhp = true;
    } else {
      this.validEhp = false;
    }
  }
  buildingDisplay(){
    /* Buildings Mapping */
    if(this.building){
      for(let building of this.building){
        if(building.lvl){
          if(building.lvl.type === "Speed"){
            this.selectSpdBuilding = building.lvl?.Speed;
            this.isSpeedBuilding = true;
          }
          if(building.lvl.type === "Fire Atk"){
            this.selectFireBuilding = building.lvl?.FireAttack;
            this.isFireAtkBuilding = true;
          }
          if(building.lvl.type === "Def"){
            this.selectDefBuilding = building.lvl?.Def;
            this.isDefBuilding = true;
          } 
          if(building.lvl.type === "Atk"){
            this.selectAttackBuilding = building.lvl?.Attack;
            this.isAtkBuilding = true;
          }
          if(building.lvl.type === "Hp"){
            this.selectHpBuilding = building.lvl?.Hp;
            this.isHpBuilding = true;
          }
          if(building.lvl.type === "C.Dmg" ){
            this.selectCritBuilding = building.lvl?.CritDamage;
            this.isCritBuilding = true;
          }
        }
      }
    } else {
      this.building = [];
    }
  }
  baleygrDisplay(){
    if(this.data) {
      for(let index of this.data.unit_list){ 
        if(index.unit_master_id.name === "Baleygr"){
          this.baleygr = index;
          this.hp =  index.unit_master_id.hp?.toFixed() - this.baleygr.unit_master_id.base_hp?.toFixed();
          this.def = this.baleygr.def?.toFixed() - this.baleygr.unit_master_id.base_def?.toFixed();
          this.spd = this.baleygr.spd?.toFixed() - this.baleygr.unit_master_id.base_spd?.toFixed();
          this.atk = this.baleygr.atk?.toFixed() - this.baleygr.unit_master_id.base_atk?.toFixed();
          this.crit_rate = this.baleygr.critical_rate?.toFixed() - this.baleygr.unit_master_id.base_crit_rate?.toFixed();
          this.crit_damage = this.baleygr.critical_damage?.toFixed() - this.baleygr.unit_master_id.base_crit_damage?.toFixed();

          for(let artifact of index.artifacts){
            for(let sec_effect of artifact.sec_effects){
              if(sec_effect.id == 204){
                this.atkPercentBonus = sec_effect.value;
              }
              if(sec_effect.id == 218){
                this.dmgBonusPerHp = sec_effect.value;
              }
              if(sec_effect.id == 219){
                this.dmgBonusPerAtk = sec_effect.value;
              }
              if(sec_effect.id == 220){
                this.dmgBonusPerDef = sec_effect.value;
              }
              if(sec_effect.id == 221){
                this.dmgBonusPerSpeed = sec_effect.value;
              }
            }
          }
        }
      }
    }
  }
  onSelect(rune): void {
    this.selectedRunes = rune;
  }
}
