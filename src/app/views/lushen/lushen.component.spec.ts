import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LushenComponent } from './lushen.component';

describe('LushenComponent', () => {
  let component: LushenComponent;
  let fixture: ComponentFixture<LushenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LushenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LushenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
