import { CDK_CONNECTED_OVERLAY_SCROLL_STRATEGY_FACTORY } from '@angular/cdk/overlay/overlay-directives';
import { Component, OnInit } from '@angular/core';
/* FontAwesome Icons */
import { faCheckCircle, faTimesCircle, faShieldAlt, faRunning, faStar, faHeart, faBahai, faPlusCircle, faEdit } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-lushen',
  templateUrl: './lushen.component.html',
  styleUrls: ['./lushen.component.scss']
})
export class LushenComponent implements OnInit {
  /* Data */
  data: any = [];
  building: any = [];
  lushen: any = [];
  /* FontAwesome Icons */
  star = faStar;
  checkCircle = faCheckCircle;
  timeCircle = faTimesCircle;
  shield = faShieldAlt;
  run = faRunning;
  heart = faHeart;
  bahai = faBahai;
  plusCircle = faPlusCircle;
  edit = faEdit;
  /* Main Lushen Stat */
  hp: number;
  def: number;
  spd: number;
  atk: number;
  crit_damage: number;
  crit_rate: number;
  requiredSpd: boolean = false;
  lushenList: any = [];
  selectedLushen: any;
  showSelectLushen: boolean = false;
  lushenSpd: number;

  lushenAtk: number;
  lushenBoostedAtk: number;
  finalDmg: number;

  selectedRunes: any;
  /* Bonus Stats */
  fightSet: number;
  guildSkill: number;
  atkLeader: number;
  atkPercentBonus: number;
  buffAtk: number;
  multiplier: number;
  reductionPassiv: number;
  reducedDmgGb: number;
  reductionGb: number;
  /* Skill Bonus */
  skillCrit: number;
  skillUp: number;
  /* Artifacts Bonus Damage */
  dmgBonusPerAtk: number;
  dmgBonusPerHp: number;
  dmgBonusPerDef: number;
  dmgBonusPerSpeed: number;
  dmgBonusFirstSkill: number;
  dmgDealtOnWater: number;
  /* Define Base Building Value */
  defBuilding = 0;
  atkBuilding = 0;
  critDamageBuilding = 0;
  fireAtkBuilding = 0;
  spdBuilding = 0;
  hpBuilding = 0;

  isAtkBuilding = false;
  isHpBuilding = false;
  isDefBuilding = false;
  isCritBuilding = false;
  isWindAtkBuilding = false;
  isSpeedBuilding = false;

  selectSpdBuilding = 0;
  selectWindBuilding = 0;
  selectAttackBuilding = 0;
  selectDefBuilding = 0;
  selectHpBuilding = 0;
  selectCritBuilding = 0;

  /* Giant B12 Stats */
  golemHp: number;
  firstHitHp: number;
  secondHitHp: number;
  thirdHitHp: number;
  isValidLushen: boolean;

  /* Building Base Select Input */
  itemSpdBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 3},
    { id: 3, value: 5},
    { id: 4, value: 6},
    { id: 5, value: 8},
    { id: 6, value: 9},
    { id: 7, value: 11},
    { id: 8, value: 12},
    { id: 9, value: 14},
    { id: 10, value: 15},
  ];
  itemWindBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 3},
    { id: 2, value: 5},
    { id: 3, value: 7},
    { id: 4, value: 9},
    { id: 5, value: 11},
    { id: 6, value: 13},
    { id: 7, value: 15},
    { id: 8, value: 17},
    { id: 9, value: 19},
    { id: 10, value: 21},
  ];
  itemAttackBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 4},
    { id: 3, value: 6},
    { id: 4, value: 8},
    { id: 5, value: 10},
    { id: 6, value: 12},
    { id: 7, value: 14},
    { id: 8, value: 16},
    { id: 9, value: 18},
    { id: 10, value: 20},
  ];
  itemCritBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 5},
    { id: 3, value: 7},
    { id: 4, value: 10},
    { id: 5, value: 12},
    { id: 6, value: 15},
    { id: 7, value: 17},
    { id: 8, value: 20},
    { id: 9, value: 22},
    { id: 10, value: 25},
  ];
  itemDefBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 4},
    { id: 3, value: 6},
    { id: 4, value: 8},
    { id: 5, value: 10},
    { id: 6, value: 12},
    { id: 7, value: 14},
    { id: 8, value: 16},
    { id: 9, value: 18},
    { id: 10, value: 20},
  ];
  itemHpBuilding = [
    { id: 0, value: 0},
    { id: 1, value: 2},
    { id: 2, value: 4},
    { id: 3, value: 6},
    { id: 4, value: 8},
    { id: 5, value: 10},
    { id: 6, value: 12},
    { id: 7, value: 14},
    { id: 8, value: 16},
    { id: 9, value: 18},
    { id: 10, value: 20},
  ];

  constructor() { }

  ngOnInit(): void {
    this.data = JSON.parse(localStorage.getItem("wizard_data"));
    this.building = JSON.parse(localStorage.getItem("wizard_building"));
    this.selectedLushen = JSON.parse(localStorage.getItem("lushen_select"));
    this.buildingDisplay();

    this.hp = 0;
    this.def = 0;
    this.spd = 0;
    this.atk = 0;
    this.crit_rate = 0;
    this.crit_damage = 0;
    this.lushenSpd = 0;
    
    this.fightSet = 0;
    this.guildSkill = 5;
    this.atkLeader = 33;
    this.atkPercentBonus = 0;
    this.skillCrit = 0;
    this.skillUp = 30;

    this.dmgBonusPerAtk = 0;
    this.dmgBonusPerHp = 0;
    this.dmgBonusPerDef = 0;
    this.dmgBonusPerSpeed = 0;
    this.dmgDealtOnWater = 0;

    this.buffAtk = 0;
    this.multiplier = 0.68;
    this.reductionPassiv = 0.6

    this.reducedDmgGb = 0;
    this.reductionGb = 1000/1140;

    this.golemHp = 26235;

    this.firstHitHp = 0
    this.secondHitHp = 0;
    this.thirdHitHp = 0;
    
    if(this.data){
      this.lushenDisplay();
    }
    this.onChange(event);
  }

  onChange(event) {
    this.requiredSpd = false;

    this.lushenAtk = Math.ceil(900 * ( 100 + this.selectWindBuilding + this.atkLeader + this.selectAttackBuilding 
      + this.guildSkill + this.fightSet * 8) / 100 + this.atk);

    let lushenDefBonus = Math.ceil(((461 * ( 100 + this.selectDefBuilding + this.guildSkill ) /100 + this.def) * this.dmgBonusPerDef)/100);
    let lushenHpBonus = Math.ceil(((9885 * ( 100 + this.selectHpBuilding + this.guildSkill ) /100 + this.hp) * this.dmgBonusPerHp)/100);
    let lushenSpdBonus = Math.ceil(((103 * ( 100 + this.selectSpdBuilding ) /100 + this.spd) * this.dmgBonusPerSpeed)/100);
    let lushenAtkBonus = Math.ceil(this.lushenAtk * this.dmgBonusPerAtk /100)

    this.lushenBoostedAtk = Math.ceil(this.lushenAtk * this.multiplier * (100 + this.skillUp + this.crit_damage 
      + 50 + this.selectCritBuilding + this.skillCrit) / 100); 

    this.finalDmg = Math.ceil(this.reductionGb * (100 + this.dmgDealtOnWater )/100 * this.lushenBoostedAtk 
      + lushenAtkBonus + lushenDefBonus + lushenSpdBonus + lushenHpBonus);

    this.firstHitHp = this.golemHp - this.finalDmg; 
    this.secondHitHp = this.firstHitHp - this.finalDmg;
    this.thirdHitHp = this.secondHitHp - (this.finalDmg * this.reductionPassiv);

    if(this.thirdHitHp <= 0) {
      this.isValidLushen = true;
    } else {
      this.isValidLushen = false;
    }

    this.lushenSpd = 103 * (100 + this.selectSpdBuilding)/ 100 + this.spd;
    if(this.lushenSpd > 169){
      this.requiredSpd = !this.requiredSpd;
    }

    this.spdBuilding = this.selectSpdBuilding;
  }

  buildingDisplay(){
    /* Buildings Mapping */
    if(this.building){
      for(let building of this.building){
        if(building.lvl){
          if(building.lvl.type === "Speed"){
            this.selectSpdBuilding = building.lvl?.Speed;
            this.isSpeedBuilding = true;
          }
          if(building.lvl.type === "Wind Atk"){
            this.selectWindBuilding = building.lvl?.WindAttack;
            this.isWindAtkBuilding = true;
          }
          if(building.lvl.type === "Def"){
            this.selectDefBuilding = building.lvl?.Def;
            this.isDefBuilding = true;
          } 
          if(building.lvl.type === "Atk"){
            this.selectAttackBuilding = building.lvl?.Attack;
            this.isAtkBuilding = true;
          }
          if(building.lvl.type === "Hp"){
            this.selectHpBuilding = building.lvl?.Hp;
            this.isHpBuilding = true;
          }
          if(building.lvl.type === "C.Dmg"){
            this.selectCritBuilding = building.lvl?.CritDamage;
            this.isCritBuilding = true;
          }
        }
      }
    } else {
      this.building = [];
    }
  }
  lushenDisplay(){
    /* Lushen */
    this.lushenList = [];
    for(let index of this.data.unit_list){
      if(index.unit_master_id.name === "Lushen"){
        this.lushenList.push(index);
        this.lushen = this.selectedLushen;
        this.hp =  this.lushen?.unit_master_id.hp.toFixed() - 9885;
        this.def = this.lushen?.def.toFixed() - this.lushen?.unit_master_id.base_def.toFixed();
        this.spd = this.lushen?.spd.toFixed() - this.lushen?.unit_master_id.base_spd.toFixed();
        this.atk = this.lushen?.atk.toFixed() - this.lushen?.unit_master_id.base_atk.toFixed();
        this.crit_rate = this.lushen?.critical_rate.toFixed() - this.lushen?.unit_master_id.base_crit_rate.toFixed();
        this.crit_damage = this.lushen?.critical_damage.toFixed() - this.lushen?.unit_master_id.base_crit_damage.toFixed();

        if(this.lushen){
          for(let artifact of this.lushen.artifacts){
            for(let sec_effect of artifact.sec_effects){
              if(sec_effect.id == 218){
                this.dmgBonusPerHp = sec_effect.value;
              }
              if(sec_effect.id == 219){
                this.dmgBonusPerAtk = sec_effect.value;
              }
              if(sec_effect.id == 220){
                this.dmgBonusPerDef = sec_effect.value;
              }
              if(sec_effect.id == 221){
                this.dmgBonusPerSpeed = sec_effect.value;
              }
              if(sec_effect.id == 400){
                this.dmgBonusFirstSkill = sec_effect.value
              }
              if(sec_effect.id == 301) {
                this.dmgDealtOnWater = sec_effect.value;
              }
              if(sec_effect.id == 402) {
                this.skillCrit = sec_effect.value;
              }
            }
          }
        }
      }
    }
    this.onChange(event);
  }
  onSelect(rune): void {
    this.selectedRunes = rune;
  }
  onLushenSelect(lushen): void {
    this.selectedLushen = lushen;
    this.lushenDisplay();
    this.showSelectLushen = true;
    localStorage.setItem("lushen_select", JSON.stringify(this.selectedLushen));
  }
  showSelect(){
    this.selectedLushen = 0;
    localStorage.removeItem('lushen_select');
    this.showSelectLushen = !this.showSelectLushen;
  }
}
