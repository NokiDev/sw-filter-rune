/* Angular*/
import { Component, OnInit, ViewChild } from "@angular/core";
import { SelectionModel } from "@angular/cdk/collections";
/* Angular Material */
import { MatPaginator } from "@angular/material/paginator";
import { MatSort } from "@angular/material/sort";
import { MatTableDataSource } from "@angular/material/table";
import {MatDialog} from '@angular/material/dialog';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from "@angular/animations";
/* FontAwesome */
import { faStar, faSlidersH } from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: "app-monsters",
  templateUrl: "./monsters.component.html",
  styleUrls: ["../../../styles.css"],
  animations: [
    trigger("detailExpand", [
      state("collapsed", style({ height: "0px", minHeight: "0" })),
      state("expanded", style({ height: "*" })),
      transition(
        "expanded <=> collapsed",
        animate("225ms cubic-bezier(0.4, 0.0, 0.2, 1)")
      ),
    ]),
  ],
})
export class MonstersComponent implements OnInit {
  isGrindStone = false;
  /* FontAwesome Icons */
  star = faStar;
  slider = faSlidersH;
  /* Table */
  data: any = [];
  monsterArtifacts: any = [];
  dataSource: any;
  displayedColumns: string[] = [
    "unit_master_id.image",
    "unit_master_id.name",
    "unit_runes",
    "unit_level",
    "atk",
    "def",
    "spd",
    "unit_master_id.hp",
    "critical_damage",
    "critical_rate",
    "resist",
    "accuracy",
  ];

  selection = new SelectionModel<any>(true, []);
  expandedElement: any;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.data = JSON.parse(localStorage.getItem("wizard_data"));
    this.monsterArtifacts = JSON.parse(localStorage.getItem("monster_artifacts"));
    this.dataSource = new MatTableDataSource(this.data.unit_list);
    this.expandedElement = this.data.unit_list;
    /* SORT FILTER FOR NESTED OBJECTS */
    this.dataSource.sortingDataAccessor = (item, property) => {
      switch (property) {
        /* MONSTERS */
        case "unit_master_id.name":
          return item.unit_master_id.name;
        case "unit_master_id.image":
          return item.unit_master_id.image;
        case "unit_master_id.hp":
          return item.unit_master_id.hp;
        case "unit_master_id.id":
          return item.unit_master_id.id;

        /* RUNES */
        case "unit_master_id.principal_set.set_name":
          return item.unit_master_id.principal_set.set_name;
        case "unit_master_id.secondary_set.set_name":
          return item.unit_master_id.secondary_set.set_name;

        default:
          return item[property];
      }
    };
    this.dataSource.filterPredicate = function (data, filter: string, filter_number: number): boolean {
      return (
        data.unit_master_id.name?.toLowerCase().includes(filter) ||
        data.atk?.toString().toLowerCase().includes(filter) ||
        data.def?.toString().toLowerCase().includes(filter) ||
        data.critical_rate?.toString().toLowerCase().includes(filter) ||
        data.critical_damage?.toString().toLowerCase().includes(filter) ||
        data.accuracy?.toString().toLowerCase().includes(filter) ||
        data.resist?.toString().toLowerCase().includes(filter) ||
        data.unit_master_id.id?.toString().toLowerCase().includes(filter)
      );
    };
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  openDialog() {
    this.dialog.open(dialogParams);
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // MatTableDataSource defaults to lowercase matches
    this.dataSource.filter = filterValue;
  }
}

@Component({
  selector: 'dialog-params',
  templateUrl: 'dialog-params.html',
  styleUrls: ["./dialog-params.scss"],
})
export class dialogParams {
  star = faStar;
}