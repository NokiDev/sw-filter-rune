export class ArtifactImport {
  data: any = [];
  artifacts: any = [];
  value: number;
  
  get artifactImport() {
    let artifact: any;
    return artifact = {
      effectTypes: {
        main: {
          100: 'HP',
          101: 'ATK',
          102: 'DEF',
        },
        rank: {
          1: 'Common',
          2: 'Magic',
          3: 'Rare',
          4: 'Hero',
          5: 'Legendary',
        },
        attribute: {
          1: 'Water',
          2: 'Fire',
          3: 'Wind',
          4: 'Light',
          5: 'Dark',
        },
        elementImg: {
          1: 'assets/img/artifacts/element-type/element_water.png',
          2: 'assets/img/artifacts/element-type/element_fire.png',
          3: 'assets/img/artifacts/element-type/element_wind.png',
          4: 'assets/img/artifacts/element-type/element_light.png',
          5: 'assets/img/artifacts/element-type/element_dark.png',
        },
        archetype: {
          1: 'Attack',
          2: 'Defense',
          3: 'HP',
          4: 'Support',
        },
        archetypeImg: {
          1: 'assets/img/artifacts/element-type/type_attack.png',
          2: 'assets/img/artifacts/element-type/type_defense.png',
          3: 'assets/img/artifacts/element-type/type_hp.png',
          4: 'assets/img/artifacts/element-type/type_support.png',
        },
      },
    };
  }
}