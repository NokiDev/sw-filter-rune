export class BuildingImport {
  data: any = [];
  buildings: any = [];

  get buildingImport() {
    let buildings: any;
    return buildings = {
      name: {
        4: "Guardstone",
        15: "Fire Sanctuary",
        31: "Fallen Ancient Guardian",
        6: "Sky Tribe Totem",
        8: "Crystal Altar",
        9: "Ancient Sword",
        16: "Water Sanctuary",
        17: "Wind Sanctuary"
      },
      img: {
        4: "./assets/img/buildings/guardstone.png",
        15: "./assets/img/buildings/fire_sanctuary.png",
        31: "./assets/img/buildings/fallen_ancient_guardian.png",
        6: "./assets/img/buildings/sky_tribe_totem.png",
        8: "./assets/img/buildings/crystal_altar.png",
        9: "./assets/img/buildings/ancient_sword.png",
        16: "./assets/img/buildings/water_sanctuary.png",
        17: "./assets/img/buildings/wind_sanctuary.png",
      },
      levelSpeed: {
        1: 2,
        2: 3,
        3: 5,
        4: 6,
        5: 8,
        6: 9,
        7: 11,
        8: 12,
        9: 14,
        10: 15,
      },
      levelAtk: {
        1: 2,
        2: 4,
        3: 6,
        4: 8,
        5: 10,
        6: 12,
        7: 14,
        8: 16,
        9: 18,
        10: 20,
      },
      levelAtkFire: {
        1: 3,
        2: 5,
        3: 7,
        4: 9,
        5: 11,
        6: 13,
        7: 15,
        8: 17,
        9: 19,
        10: 21,
      },
      levelCritDamage: {
        1: 2,
        2: 5,
        3: 7,
        4: 10,
        5: 12,
        6: 15,
        7: 17,
        8: 20,
        9: 22,
        10: 25,
      },
      guardStone: {
        1: 2,
        2: 4,
        3: 6,
        4: 8,
        5: 10,
        6: 12,
        7: 14,
        8: 16,
        9: 18,
        10: 20,
      },
      crystalAltar: {
        1: 2,
        2: 4,
        3: 6,
        4: 8,
        5: 10,
        6: 12,
        7: 14,
        8: 16,
        9: 18,
        10: 20,
      },
      levelAtkWater: {
        1: 3,
        2: 5,
        3: 7,
        4: 9,
        5: 11,
        6: 13,
        7: 15,
        8: 17,
        9: 19,
        10: 21,
      },
      levelAtkWind: {
        1: 3,
        2: 5,
        3: 7,
        4: 9,
        5: 11,
        6: 13,
        7: 15,
        8: 17,
        9: 19,
        10: 21,
      }
    };
  }
}