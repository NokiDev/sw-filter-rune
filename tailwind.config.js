const colors = require('tailwindcss/colors')

module.exports = {
  purge: {
    enabled: true,
    content: ['./src/**/*.html', './src/**/*.ts'],
  },
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    colors: {
      transparent: 'transparent',
      current: 'currentColor',
      black: colors.black,
      white: colors.white,
      gray: colors.trueGray,
      indigo: colors.indigo,
      red: colors.rose,
      yellow: colors.amber,
      warmGrey: colors.warmGray,
      accent: {
        light: '#ff0055',
        DEFAULT: '#8d4c62',
        dark: '#680023',
      },
      sw: {
        brown: '#271707',
        softBrown: '#35230f',
        green: '#c4e48d',
        orange: '#f7ac51',
        yellow: '#fbeda8',
        blue: '#00879d',
      },
      clearBlack: '#121212',
    },
    fontFamily: {
      sans: ['Graphik', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
    extend: {
      spacing: {
        '128': '32rem',
        '144': '36rem',
      },
      borderRadius: {
        '4xl': '2rem',
      },
      backgroundImage: theme => ({
        'hero-background': "url('/assets/img/header/bg_sw_colored.jpg')",
        'import-background': "url('/assets/img/header/sw-header-import.jpg')",
        'game-info-background': "url('/assets/img/header/sw-header-worldmap.jpg')",
        'monster-background': "url('/assets/img/header/sw-header-monsters.jpg')",
        'rune-background': "url('/assets/img/header/sw-header-mana.jpg')",
      })
    },
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1024px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
    }
  }
}